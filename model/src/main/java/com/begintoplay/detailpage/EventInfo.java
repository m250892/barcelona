package com.begintoplay.detailpage;

import java.io.Serializable;

/**
 * Created by manoj on 07/01/17.
 */

public class EventInfo implements Serializable {
  private String name;
  private String date;
  private String price;

  public String getName() {
    return name;
  }

  public String getDate() {
    return date;
  }

  public String getPrice() {
    return price;
  }
}
