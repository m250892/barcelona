package com.begintoplay.detailpage;

import com.begintoplay.cardtemplate.BaseTemplateModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by manoj on 07/01/17.
 */

public interface EventDetailPageDTO extends Serializable{

  EventInfo getInfo();

  List<String> getGallery();

  List<BaseTemplateModel> getCards();
}
