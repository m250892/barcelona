package com.begintoplay.detailpage;

import com.begintoplay.cardtemplate.BaseTemplateModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by manoj on 07/01/17.
 */

public class EventDetailPageResponse implements EventDetailPageDTO {
  private EventInfo info;
  private List<String> gallery;
  private Map<String, BaseTemplateModel> cards;
  @SerializedName("card_order")
  private List<String> cardOrder;

  public EventInfo getInfo() {
    return info;
  }

  public List<String> getGallery() {
    return gallery;
  }

  public List<BaseTemplateModel> getCards() {
    List<BaseTemplateModel> result = new ArrayList<>();
    for (String cardId : cardOrder) {
      result.add(cards.get(cardId));
    }
    return result;
  }
}
