package com.begintoplay.cardtemplate;

import com.begintoplay.parser.DataTypeProvider;
import com.begintoplay.adapter.CardTemplateAdapter;
import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;

/**
 * Created by manoj on 07/01/17.
 */

@JsonAdapter(CardTemplateAdapter.class)
public abstract class BaseTemplateModel implements DataTypeProvider, Serializable {

  @Override
  public String getDataType() {
    return getCardType().getType();
  }

  public abstract TemplateCardType getCardType();
}
