package com.begintoplay.cardtemplate;

public enum TemplateCardType {
  /**
   * TEMPLATE_CARD_ONE ==> About card, Heading and description with more info page
   * TEMPLATE_CARD_TWO ==> Payment/Cost Info, Title button and onClick of this open detail page
   * TEMPLATE_CARD_THIRD ==> Description, Grid page with 2*N
   * TEMPLATE_CARD_FOURTH ==> Inclusion and exclusion card
   * TEMPLATE_CARD_FIVE ==> Check list page
   */
  TEMPLATE_CARD_ONE("TEMPLATE_CARD_ONE"),
  TEMPLATE_CARD_TWO("TEMPLATE_CARD_TWO"),
  TEMPLATE_CARD_THIRD("TEMPLATE_CARD_THIRD"),
  TEMPLATE_CARD_FOURTH("TEMPLATE_CARD_FOURTH"),
  TEMPLATE_CARD_FIVE("TEMPLATE_CARD_FIVE");

  private final String type;

  TemplateCardType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}