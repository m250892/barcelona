package com.begintoplay.cardtemplate;

import java.util.List;

/**
 * Created by manoj on 07/01/17.
 */

public class TemplateFourthModel extends BaseTemplateModel {

  private List<KeyContentModel> data;

  public List<KeyContentModel> getData() {
    return data;
  }

  @Override
  public TemplateCardType getCardType() {
    return TemplateCardType.TEMPLATE_CARD_FOURTH;
  }
}
