package com.begintoplay.cardtemplate;

import java.io.Serializable;
import java.util.List;

/**
 * Created by manoj on 08/01/17.
 */

public class KeyContentModel implements Serializable {
  private String title;
  private List<String> content;

  public String getTitle() {
    return title;
  }

  public List<String> getContent() {
    return content;
  }
}
