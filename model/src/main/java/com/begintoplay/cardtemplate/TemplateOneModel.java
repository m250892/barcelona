package com.begintoplay.cardtemplate;

/**
 * Created by manoj on 07/01/17.
 */

public class TemplateOneModel extends BaseTemplateModel {

  private String title;
  private String description;

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public TemplateCardType getCardType() {
    return TemplateCardType.TEMPLATE_CARD_ONE;
  }
}
