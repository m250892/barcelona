package com.begintoplay.cardtemplate;

import java.util.List;

/**
 * Created by manoj on 07/01/17.
 */

public class TemplateFiveModel extends BaseTemplateModel {
  private String title;
  private List<String> data;

  public TemplateFiveModel(String title, List<String> data) {
    this.title = title;
    this.data = data;
  }

  public String getTitle() {
    return title;
  }

  public List<String> getData() {
    return data;
  }

  @Override
  public TemplateCardType getCardType() {
    return TemplateCardType.TEMPLATE_CARD_FIVE;
  }
}
