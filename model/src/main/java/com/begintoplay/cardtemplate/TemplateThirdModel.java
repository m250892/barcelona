package com.begintoplay.cardtemplate;

import java.util.List;

/**
 * Created by manoj on 07/01/17.
 */

public class TemplateThirdModel extends BaseTemplateModel {

  private List<KeyValuePair> data;

  public List<KeyValuePair> getData() {
    return data;
  }

  @Override
  public TemplateCardType getCardType() {
    return TemplateCardType.TEMPLATE_CARD_THIRD;
  }
}
