package com.begintoplay.cardtemplate;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manoj on 07/01/17.
 */

public class KeyValuePair implements Serializable {
  @SerializedName("heading")
  private String key;
  @SerializedName("description")
  private String value;

  public String getKey() {
    return key;
  }

  public String getValue() {
    return value;
  }
}
