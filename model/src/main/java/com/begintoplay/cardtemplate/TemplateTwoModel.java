package com.begintoplay.cardtemplate;

import java.util.List;

/**
 * Created by manoj on 07/01/17.
 */

public class TemplateTwoModel extends BaseTemplateModel {

  private String title;
  private List<KeyValuePair> data;

  public String getTitle() {
    return title;
  }

  public List<KeyValuePair> getData() {
    return data;
  }

  @Override
  public TemplateCardType getCardType() {
    return TemplateCardType.TEMPLATE_CARD_TWO;
  }
}
