package com.begintoplay.parser;

public interface DataTypeProvider {

    String getDataType();
}