package com.begintoplay.util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by manoj on 07/01/17.
 */

public class GsonUtil {

  public static String toJson(Object object) {
    Gson gson = new Gson();
    return gson.toJson(object);
  }

  public static <T> T fromJson(String jsonString, Class<T> clazz)
   throws JsonSyntaxException {
    Gson gson = new Gson();
    return gson.fromJson(jsonString, clazz);
  }
}