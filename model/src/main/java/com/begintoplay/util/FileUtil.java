package com.begintoplay.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by manoj on 07/01/17.
 */

public class FileUtil {

  public static String loadData(Context context, int resourceId) {
    InputStream is = context.getResources().openRawResource(resourceId);
    return readDataFromInputStream(is);
  }

  private static String readDataFromInputStream(InputStream inputStream) {
    try {
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
      StringBuilder total = new StringBuilder();
      String line;
      while ((line = bufferedReader.readLine()) != null) {
        total.append(line);
      }
      return total.toString();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}
