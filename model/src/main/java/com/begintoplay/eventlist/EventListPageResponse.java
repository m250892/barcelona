package com.begintoplay.eventlist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manoj on 09/01/17.
 */

public class EventListPageResponse implements Serializable {
  private List<EventList> data;

  public EventListPageResponse(List<EventList> data) {
    this.data = data;
  }

  public List<EventCardDto> getEventCardList() {
    List<EventCardDto> result = new ArrayList<>();
    result.addAll(data);
    return result;
  }
}
