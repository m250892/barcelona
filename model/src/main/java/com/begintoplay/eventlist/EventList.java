package com.begintoplay.eventlist;

import com.begintoplay.detailpage.EventInfo;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manoj on 09/01/17.
 */

public class EventList extends EventInfo implements EventCardDto {
  @SerializedName("cover_image")
  private String coverUrl;
  private List<String> tags;

  public String getCoverUrl() {
    return coverUrl;
  }

  @Override
  public List<String> getTagList() {
    return tags;
  }
}
