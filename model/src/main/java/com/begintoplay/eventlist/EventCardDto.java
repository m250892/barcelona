package com.begintoplay.eventlist;

import java.util.List;

/**
 * Created by manoj on 09/01/17.
 */

public interface EventCardDto {
  String getName();

  String getDate();

  String getPrice();

  String getCoverUrl();

  List<String> getTagList();
}
