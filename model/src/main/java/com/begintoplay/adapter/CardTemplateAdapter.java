package com.begintoplay.adapter;

import com.begintoplay.parser.DataTypeAdapterFactory;
import com.begintoplay.parser.DataTypeProvider;
import com.begintoplay.cardtemplate.TemplateCardType;
import com.begintoplay.cardtemplate.TemplateFiveModel;
import com.begintoplay.cardtemplate.TemplateFourthModel;
import com.begintoplay.cardtemplate.TemplateOneModel;
import com.begintoplay.cardtemplate.TemplateThirdModel;
import com.begintoplay.cardtemplate.TemplateTwoModel;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by manoj on 07/01/17.
 */

public class CardTemplateAdapter extends DataTypeAdapterFactory {
  private static final String GSON_MAPPING_KEY = "type";

  private static final Map<String, Class<? extends DataTypeProvider>> classMappingKeyValue =
   new LinkedHashMap<String, Class<? extends DataTypeProvider>>() {
     {
       put(TemplateCardType.TEMPLATE_CARD_ONE.getType(), TemplateOneModel.class);
       put(TemplateCardType.TEMPLATE_CARD_TWO.getType(), TemplateTwoModel.class);
       put(TemplateCardType.TEMPLATE_CARD_THIRD.getType(), TemplateThirdModel.class);
       put(TemplateCardType.TEMPLATE_CARD_FOURTH.getType(), TemplateFourthModel.class);
       put(TemplateCardType.TEMPLATE_CARD_FIVE.getType(), TemplateFiveModel.class);
     }
   };

  public CardTemplateAdapter() {
    super(classMappingKeyValue, GSON_MAPPING_KEY);
  }
}
