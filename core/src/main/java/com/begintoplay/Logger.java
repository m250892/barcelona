package com.begintoplay;

import android.util.Log;

/**
 * Created by manoj on 07/01/17.
 */

public class Logger {

  private static final String TAG = "manoj";

  public static void print(String message) {
    Log.d(TAG, message);
  }
}
