package com.begintoplay.ui;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.begintoplay.interfaces.IPermissionsCallback;

/**
 * Created by manoj on 14/02/16.
 */
public interface IBaseFragmentController {

  Context getBaseContext();

  void performOperation(final int operation, Object input);

  Fragment getCurrentFragment();

  Fragment getTopFragmentInBackStack();

  boolean isForeground();

  void popBackStackIfForeground();

  void popBackStack();

  void removeCurrentFragment();

  void requestPermission(int requestCode, IPermissionsCallback permissionsCallback);

  void unregisterForPermissionRequest(int requestCode);

  void onBackPressed();

  ActionBar getSupportActionBarM();

  void setSupportActionBarM(Toolbar toolbar);

  void intigrateToolbarWithDrawer(Toolbar toolbar);
}
