package com.begintoplay.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.begintoplay.Logger;
import com.begintoplay.util.ScreenUtils;

/**
 * Created by manoj on 14/02/16.
 */
public abstract class BaseDialog extends DialogFragment implements View.OnClickListener {

  private Activity activityReference;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    this.activityReference = activity;
  }

  @Override
  public void onStart() {
    super.onStart();
    if (null != getDialog() && isFullScreen()) {
      getDialog().getWindow().setLayout(ScreenUtils.getScreenWidthPx(), ScreenUtils.getScreenHeightPx());
    }
  }


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View baseView = inflater.inflate(getLayoutResourceId(), container, false);
    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    //If dialog is fullScreen
    initViews(baseView);
    if (isFullScreen()) {
      getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getContext(), android.R.color.white)));
    }

    return baseView;
  }

  @Override
  public void onClick(View v) {

  }

  public void hideKeyboard() {
    View view = getActivityReference().getCurrentFocus();
    if (view != null) {
      ((InputMethodManager) getActivityReference().getSystemService(Context.INPUT_METHOD_SERVICE)).
                                                                                                   hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
  }

  protected void hideSoftKeyBoard(EditText et) {
    if (null != et && getActivityReference() != null) {
      InputMethodManager imm = (InputMethodManager) getActivityReference()
                                                     .getSystemService(Activity.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
    }
  }

  protected void showSoftKeyBoard(EditText et) {
    if (null != et && getActivityReference() != null) {
      InputMethodManager imm = (InputMethodManager) getActivityReference()
                                                     .getSystemService(Activity.INPUT_METHOD_SERVICE);
      imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }
  }

  public void show(FragmentManager manager, String tag) {
    if (null != manager && manager.findFragmentByTag(tag) == null) {
      try {
        super.show(manager, tag);
      } catch (IllegalStateException ise) {
        // Caused by commiting ft after onSaveInstanceState which happens in super.show(),
        // it is a bug by android as sometimes show is triggered by onClick of some UI
        // element after MainActivity is paused
        Logger.print(ise.getMessage() + ",IllegalStateException while showing DialogFragment - " + ise.getMessage());
      }
    }
  }

  public void show() {
    show(getChildFragmentManager(), this.getClass().getCanonicalName());
  }

  public Activity getActivityReference() {
    return activityReference;
  }

  protected boolean isFullScreen() {
    return false;
  }

  protected abstract void initViews(View baseView);

  protected abstract int getLayoutResourceId();

}
