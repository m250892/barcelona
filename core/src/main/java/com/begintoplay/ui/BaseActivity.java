package com.begintoplay.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.begintoplay.core.R;
import com.begintoplay.interfaces.IPermissionsCallback;
import com.begintoplay.storage.SharedPrefsManager;

import java.util.HashMap;


/**
 * Created by manoj on 14/02/16.
 */
public abstract class BaseActivity extends AppCompatActivity implements IBaseFragmentController {
  private FragmentTransaction fragmentTransaction;
  protected HashMap<Integer, IPermissionsCallback> requestCodeToCallbackMap = new HashMap<>();

  @Override
  public boolean isForeground() {
    return false;
  }

  @Override
  public Fragment getCurrentFragment() {
    return getSupportFragmentManager().findFragmentById(getFragmentContainerId());
  }

  @Override
  public Fragment getTopFragmentInBackStack() {
    int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
    if (backStackEntryCount <= 0) {
      return null;
    }
    FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager().getBackStackEntryAt(backStackEntryCount - 1);
    String fragmentTag = backStackEntry.getName();
    if (TextUtils.isEmpty(fragmentTag)) {
      throw new IllegalStateException("Fragment added without a tag");
    }
    return getSupportFragmentManager().findFragmentByTag(fragmentTag);
  }

  @Override
  public void popBackStackIfForeground() {
    if (getSupportFragmentManager() != null && isForeground()) {
      getSupportFragmentManager().popBackStack();
    }
  }

  @Override
  public void popBackStack() {
    if (getSupportFragmentManager() != null) {
      getSupportFragmentManager().popBackStack();
    }
  }

  @Override
  public void removeCurrentFragment() {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    Fragment currentFrag = getCurrentFragment();
    if (currentFrag != null) {
      transaction.remove(currentFrag);
    }
    transaction.commit();
  }

  public void clearBackStack(boolean isInclusive) {
    if (getSupportFragmentManager() != null) {
      int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
      if (backStackEntryCount <= 0) {
        return;
      }
      FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager()
                                                       .getBackStackEntryAt(0);
      String fragmentTag = backStackEntry.getName();
      if (isInclusive) {
        getSupportFragmentManager()
         .popBackStack(fragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
      } else {
        getSupportFragmentManager().popBackStack(fragmentTag, 0);
      }
    }
  }

  public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded) {
    // Allow state loss by default
    addFragmentInDefaultLayout(fragmentToBeLoaded, true, true);
  }

  public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                         boolean addToBackStack) {
    // Allow state loss by default
    addFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, true);
  }

  public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded, boolean addToBackStack,
                                         boolean allowStateLoss) {
    addFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, allowStateLoss, getSupportFragmentManager());
  }


  public void addFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded, boolean addToBackStack,
                                         boolean allowStateLoss, FragmentManager fragmentManager) {
    if (!getSupportFragmentManager().isDestroyed()) {
      fragmentTransaction = fragmentManager.beginTransaction();
      fragmentTransaction
       .add(getFragmentContainerId(), fragmentToBeLoaded, fragmentToBeLoaded.getName());
      if (addToBackStack) {
        fragmentTransaction.addToBackStack(fragmentToBeLoaded.getName());
      }
      Fragment currFragment = getCurrentFragment();
      if (null != currFragment) {
        currFragment.onPause();
      }
      if (allowStateLoss) {
        fragmentTransaction.commitAllowingStateLoss();
      } else {
        fragmentTransaction.commit();
      }
    }
  }

  public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded) {
    // Allow state loss by default
    replaceFragmentInDefaultLayout(fragmentToBeLoaded, true, true);
  }

  public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                             boolean addToBackStack) {
    // Allow state loss by default
    replaceFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, true);
  }

  public void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded,
                                             boolean addToBackStack, boolean allowStateLoss) {
    replaceFragmentInDefaultLayout(fragmentToBeLoaded, addToBackStack, allowStateLoss, getSupportFragmentManager());
  }

  private void replaceFragmentInDefaultLayout(BaseFragment fragmentToBeLoaded, boolean addToBackStack, boolean allowStateLoss, FragmentManager supportFragmentManager) {
    if (!getSupportFragmentManager().isDestroyed()) {
      fragmentTransaction = supportFragmentManager.beginTransaction();
      //fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
      fragmentTransaction.replace(getFragmentContainerId(), fragmentToBeLoaded,
       fragmentToBeLoaded.getName());
      if (addToBackStack) {
        fragmentTransaction.addToBackStack(fragmentToBeLoaded.getName());
      }
      if (allowStateLoss) {
        fragmentTransaction.commitAllowingStateLoss();
      } else {
        fragmentTransaction.commit();
      }
    }
  }

  protected int getStatusBarHeight() {
    int statusBarHeight = 0;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
      if (resourceId > 0) {
        statusBarHeight = getResources().getDimensionPixelSize(resourceId);
      }
    }
    return statusBarHeight;
  }


  @Override
  public void requestPermission(int requestCode, IPermissionsCallback permissionsCallback) {
    String permission = null;
    switch (requestCode) {
      case IPermissionsCallback.GET_ACCOUNTS:
        permission = Manifest.permission.GET_ACCOUNTS;
        break;

      case IPermissionsCallback.GET_LOCATION:
        permission = Manifest.permission.ACCESS_FINE_LOCATION;
        break;
      default:
        new RuntimeException("Permission not find in callbacks");
    }
    requestPermission(requestCode, permission, permissionsCallback);
  }

  protected void requestPermission(int requestCode, String permission, IPermissionsCallback permissionsCallback) {
    if (checkPermissionGranted(permission)) {
      permissionsCallback.onPermissionRequestResult(requestCode, true);
    } else {
      requestCodeToCallbackMap.put(requestCode, permissionsCallback);
      if (shouldShowRequestPermissionRationaleDialog(permission)) {
        showOpenSettingDialog(requestCode);
      } else {
        ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
      }
      SharedPrefsManager.getInstance().setBoolean(permission, true);
    }
  }

  private void showOpenSettingDialog(final int requestCode) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(R.string.open_setting_screen)
     .setPositiveButton(R.string.open, new DialogInterface.OnClickListener() {
       public void onClick(DialogInterface dialog, int id) {
         openAppSettingScreen();
       }
     })
     .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
       public void onClick(DialogInterface dialog, int id) {
         onRequestPermissionsResult(requestCode, false);
       }
     });
    AlertDialog dialog = builder.create();
    dialog.show();
  }

  private void openAppSettingScreen() {
    final Intent i = new Intent();
    i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    i.addCategory(Intent.CATEGORY_DEFAULT);
    i.setData(Uri.parse("package:" + this.getPackageName()));
    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
    i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
    this.startActivity(i);
  }

  public boolean checkPermissionGranted(String permission) {
    return ActivityCompat.checkSelfPermission(this, permission) ==
            PackageManager.PERMISSION_GRANTED;
  }

  @Override
  public void unregisterForPermissionRequest(int requestCode) {
    requestCodeToCallbackMap.remove(requestCode);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                         int[] grantResults) {
    onRequestPermissionsResult(requestCode, grantResults[0] ==
                                             PackageManager.PERMISSION_GRANTED);
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  public void onRequestPermissionsResult(int requestCode, boolean isGranded) {
    IPermissionsCallback permissionsCallback = requestCodeToCallbackMap.get(requestCode);
    if (permissionsCallback != null) {
      unregisterForPermissionRequest(requestCode);
      permissionsCallback.onPermissionRequestResult(requestCode, isGranded);
    }
  }

  public boolean shouldShowRequestPermissionRationaleDialog(String permission) {
    if (checkPermissionGranted(permission)) {
      return false;
    }
    boolean haveAskedBefore = SharedPrefsManager.getInstance().getBoolean(permission);
    boolean showRationale = ActivityCompat
                             .shouldShowRequestPermissionRationale(this, permission);

    return haveAskedBefore && !showRationale;
    //return showRationale;
  }

  @Override
  public ActionBar getSupportActionBarM() {
    return getSupportActionBar();
  }

  @Override
  public void setSupportActionBarM(Toolbar toolbar) {
    setSupportActionBar(toolbar);
  }

  @Override
  public void intigrateToolbarWithDrawer(Toolbar toolbar) {
  }

  public abstract int getFragmentContainerId();
}
