package com.begintoplay.ui;

import android.app.Application;
import android.content.Context;

import com.begintoplay.storage.SharedPrefsManager;


/**
 * Created by manoj on 14/02/16.
 */
public class BaseApplication extends Application {

  private static Context context;

  @Override
  public void onCreate() {
    context = this.getBaseContext();
    SharedPrefsManager.initialize(this);
    super.onCreate();
  }

  public static Context getContext() {
    return context;
  }
}
