package com.begintoplay.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;


/**
 * Created by manoj on 14/02/16.
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {

  protected Activity activity;
  private IBaseFragmentController baseFragmentControllerActivity;

  public Context getContext() {
    return activity;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.bind(this, view);
    initViews(view);
  }

  public void setToolbar(Toolbar toolbar) {
    getFragmentController().setSupportActionBarM(toolbar);
    getFragmentController().getSupportActionBarM().setDisplayShowTitleEnabled(false);
    getFragmentController().getSupportActionBarM().setDisplayHomeAsUpEnabled(true);
  }

  public void setToolbarTitle(String toolbarTitle) {
    getFragmentController().getSupportActionBarM().setTitle(toolbarTitle);
    getFragmentController().getSupportActionBarM().setDisplayShowTitleEnabled(true);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        getFragmentController().onBackPressed();
        break;
    }
    return super.onOptionsItemSelected(item);
  }


  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    try {
      this.activity = activity;
      baseFragmentControllerActivity = (IBaseFragmentController) activity;
    } catch (ClassCastException cce) {
      throw new ClassCastException(getName() + " Fragment's parent activity: " + activity.toString() +
                                    "must implement IFragmentController");
    }
  }

  public String getName() {
    return this.getClass().getCanonicalName();
  }

  public IBaseFragmentController getFragmentController() {
    if (baseFragmentControllerActivity == null) {
      baseFragmentControllerActivity = (IBaseFragmentController) getActivity();
    }
    return baseFragmentControllerActivity;
  }

  protected Activity getActivityReference() {
    if (activity == null) {
      activity = getActivity();
    }
    return activity;
  }

  @Override
  public void onDetach() {
    this.activity = null;
    this.baseFragmentControllerActivity = null;
    super.onDetach();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View baseView = inflater.inflate(getLayoutResourceId(), container, false);
    return baseView;
  }

  public void hideKeyboard() {
    View view = getActivityReference().getCurrentFocus();
    if (view != null) {
      ((InputMethodManager) getActivityReference().getSystemService(Context.INPUT_METHOD_SERVICE)).
                                                                                                   hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
  }

  protected void hideSoftKeyBoard(EditText et) {
    if (null != et && getActivityReference() != null) {
      InputMethodManager imm = (InputMethodManager) getActivityReference()
                                                     .getSystemService(Activity.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
    }
  }

  protected void showSoftKeyBoard(EditText et) {
    if (null != et && getActivityReference() != null) {
      InputMethodManager imm = (InputMethodManager) getActivityReference()
                                                     .getSystemService(Activity.INPUT_METHOD_SERVICE);
      imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }
  }

  public void showToastMessage(String message) {
    Toast.makeText(getActivityReference(), message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onDestroyView() {
    hideKeyboard();
    super.onDestroyView();
  }

  @Override
  public void onClick(View v) {
  }

  public boolean handleBackPress() {
    return false;
  }

  protected abstract int getLayoutResourceId();

  protected abstract void initViews(View baseView);


}
