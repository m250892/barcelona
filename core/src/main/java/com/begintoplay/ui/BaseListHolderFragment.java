package com.begintoplay.ui;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.begintoplay.core.R;

/**
 * Created by manoj on 19/02/16.
 */
public abstract class BaseListHolderFragment extends BaseFragment {
  protected ViewPager viewPager;
  protected TabLayout tabLayout;
  protected Toolbar toolbar;

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_base_list_holder_layout;
  }

  @Override
  protected void initViews(View baseView) {
    toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
    setToolbar(toolbar);

    viewPager = (ViewPager) baseView.findViewById(R.id.tabanim_viewpager);
    setViewPagerAdapter();
    tabLayout = (TabLayout) baseView.findViewById(R.id.tabanim_tabs);
    tabLayout.setupWithViewPager(viewPager);
  }

  protected abstract void setViewPagerAdapter();

}
