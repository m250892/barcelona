package com.begintoplay.view;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoBoldButton extends CustomButton {

    public static final String FONT_NAME = "fonts/Roboto-Bold.ttf";

    public RobotoBoldButton(Context context) {
        super(context);
    }

    public RobotoBoldButton(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public RobotoBoldButton(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
    }

    @Override
    protected String getFontName() {
        return FONT_NAME;
    }

}
