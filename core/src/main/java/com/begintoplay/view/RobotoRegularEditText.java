package com.begintoplay.view;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoRegularEditText extends CustomEditText {

    public static final String FONT_NAME = "fonts/Roboto-Regular.ttf";


    public RobotoRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RobotoRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public RobotoRegularEditText(Context context) {
        super(context);
    }

    @Override
    protected String getFontName() {
        return FONT_NAME;
    }
}
