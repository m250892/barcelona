
package com.begintoplay.view;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoBoldTextView extends CustomTextView {

    public static final String FONT_NAME = "fonts/Roboto-Bold.ttf";

    public RobotoBoldTextView(Context context) {
        super(context);
    }

    public RobotoBoldTextView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public RobotoBoldTextView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
    }

    @Override
    protected String getFontName() {
        return FONT_NAME;
    }
}
