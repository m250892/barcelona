package com.begintoplay.view;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoMediumTextView extends CustomTextView {

    public static final String FONT_NAME = "fonts/Roboto-Medium.ttf";


    public RobotoMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RobotoMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public RobotoMediumTextView(Context context) {
        super(context);
    }

    @Override
    protected String getFontName() {
        return FONT_NAME;
    }
}
