package com.begintoplay.view;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoMediumButton extends CustomButton {

    public static final String FONT_NAME = "fonts/Roboto-Medium.ttf";

    public RobotoMediumButton(Context context) {
        super(context);
    }

    public RobotoMediumButton(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public RobotoMediumButton(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
    }

    @Override
    protected String getFontName() {
        return FONT_NAME;
    }
}
