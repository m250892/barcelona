package com.begintoplay.view;

import android.content.Context;
import android.util.AttributeSet;

public class RobotoRegularButton extends CustomButton {

    public static final String FONT_NAME = "fonts/Roboto-Regular.ttf";

    public RobotoRegularButton(Context context) {
        super(context);
    }

    public RobotoRegularButton(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public RobotoRegularButton(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
    }

    @Override
    protected String getFontName() {
        return FONT_NAME;
    }

}
