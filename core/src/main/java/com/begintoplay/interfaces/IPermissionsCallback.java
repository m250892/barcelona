package com.begintoplay.interfaces;

public interface IPermissionsCallback {
  int GET_ACCOUNTS = 1;
  int GET_LOCATION = 2;
  int CAMERA = 3;
  int EXTERNAL_STORAGE = 4;

  void onPermissionRequestResult(int requestCode, boolean result);
}