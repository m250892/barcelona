package com.begintoplay.util;

import com.begintoplay.Logger;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class MathUtils {

  private static final float DEG_TO_RAD = 3.1415926f / 180.0f;
  private static final float RAD_TO_DEG = 180.0f / 3.1415926f;

  protected MathUtils() {
  }

  /**
   * Rounds a double value to a certain number of digits
   *
   * @param toBeRounded number to be rounded
   * @param digits      number of digits to be rounded
   * @return the double rounded
   */
  public static double round(double toBeRounded, int digits) {
    if (digits < 0) {
      Logger.print("must be greater than 0");
      return 0;
    }
    String formater = "";
    for (int i = 0; i < digits; i++) {
      formater += "#";
    }

    DecimalFormat twoDForm = new DecimalFormat("#." + formater, new DecimalFormatSymbols(Locale.US));
    return Double.valueOf(twoDForm.format(toBeRounded));
  }

  public static float interpolate(float x1, float x2, float f) {
    return x1 + (x2 - x1) * f;
  }

  public static float uninterpolate(float x1, float x2, float v) {
    if (x2 - x1 == 0) {
      throw new IllegalArgumentException("Can't reverse interpolate with domain size of 0");
    }
    return (v - x1) / (x2 - x1);
  }

  public static float dist(float x, float y) {
    return (float) Math.sqrt(x * x + y * y);
  }

  public static int floorEven(int num) {
    return num & ~0x01;
  }

  public static int roundMult4(int num) {
    return (num + 2) & ~0x03;
  }

  // divide two integers but round up
  // see http://stackoverflow.com/a/7446742/102703
  public static int intDivideRoundUp(int num, int divisor) {
    int sign = (num > 0 ? 1 : -1) * (divisor > 0 ? 1 : -1);
    return sign * (Math.abs(num) + Math.abs(divisor) - 1) / Math.abs(divisor);
  }

  /**
   * Returns a random integer between MIN inclusive and MAX inclusive.
   *
   * @param min value inclusive
   * @param max value inclusive
   * @return an int between MIN inclusive and MAX exclusive.
   */
  public static int getRandomInteger(int min, int max) {
    Random r = new Random();
    return r.nextInt(max - min + 1) + min;
  }

  /**
   * Returns a random integer between 0 (Zero) inclusive and MAX inclusive. <br/>
   * Same as {@code getRandomInteger(0, max);} <br/>
   * See {@see RandomUtil#getRandomInteger(int, int)}
   *
   * @param max value exclusive
   * @return an int between 0 inclusive and MAX inclusive.
   */
  public static int getRandomInteger(int max) {
    return getRandomInteger(0, max);
  }

  /**
   * Returns a random double between MIN inclusive and MAX inclusive.
   *
   * @param min value inclusive
   * @param max value inclusive
   * @return an int between 0 inclusive and MAX exclusive.
   */
  public static double getRandomDouble(double min, double max) {
    Random r = new Random();
    return min + (max - min) * r.nextDouble();
  }

  /**
   * Returns a random double between 0 (Zero) inclusive and MAX inclusive. <br/>
   * Same as {@code getRandomDouble(0, max);} <br/>
   * See {@see RandomUtil#getRandomDouble(double, double)}
   *
   * @param max value exclusive
   * @return an int between 0 inclusive and MAX inclusive.
   */
  public static double getRandomDouble(double max) {
    return getRandomDouble(0, max);
  }

  /**
   * Get a random position(object) from an array of generic objects. <br/>
   * Using generics saves the trouble of casting the return object.
   *
   * @param <T>   the type of the array to get the object from
   * @param array the array with objects
   * @return random object from given array or null of array is either null or
   * empty
   */
  public static <T> T getRandomPosition(T[] array) {
    if (array == null || array.length == 0) {
      return null;
    }
    return array[getRandomInteger(array.length - 1)];
  }

  /**
   * Get a random position(object) from a list of generic objects. <br/>
   * Using generics saves the trouble of casting the return object.
   *
   * @param <T>  the type of the list objects to get the object from
   * @param list the list with objects
   * @return random object from given list or null of list is either null or
   * empty
   */
  public static <T> T getRandomPosition(List<T> list) {
    if (list == null || list.isEmpty()) {
      return null;
    }
    return list.get(getRandomInteger(list.size() - 1));
  }

  /**
   * Degrees to radians
   *
   * @param degrees
   * @return the converted value
   */
  public static float degreesToRadians(float degrees) {
    return degrees * DEG_TO_RAD;
  }

  /**
   * Radians to degrees
   *
   * @param radians
   * @return the converted value
   */
  public static float radiansToDegrees(float radians) {
    return radians * RAD_TO_DEG;
  }

  /**
   * Absolute value
   *
   * @param v value
   * @return returns the absolute value
   */
  public static float abs(float v) {
    return v > 0 ? v : -v;
  }

  /**
   * Gets the higher number
   *
   * @param a float number
   * @param b float number
   * @return the higher number between a and b
   */
  public static float max(float a, float b) {
    return a > b ? a : b;
  }

  /**
   * Gets the higher number
   *
   * @param a int number
   * @param b int number
   * @return the higher number between a and b
   */
  public static int max(int a, int b) {
    return a > b ? a : b;
  }

  /**
   * Gets the lower number
   *
   * @param a float number
   * @param b float number
   * @return the lower number between a and b
   */
  public static float min(float a, float b) {
    return a < b ? a : b;
  }

  /**
   * Gets the lower number
   *
   * @param a float number
   * @param b float number
   * @return the lower number between a and b
   */
  public static int min(int a, int b) {
    return a < b ? a : b;
  }

  /**
   * Check if a number is Odd
   *
   * @param num int number
   * @return true if the num is odd and false if it's even
   */
  public static boolean isOdd(int num) {
    return !isEven(num);
  }

  /**
   * Check if a number is Even
   *
   * @param num int number
   * @return true if the num is even and false if it's odd
   */
  public static boolean isEven(int num) {
    return (num % 2 == 0);
  }

  /**
   * Truncates a value
   *
   * @param value  - value to be truncated
   * @param places - decimal places
   * @return
   */
  public static double truncate(double value, int places) {
    if (places < 0) {
      throw new IllegalArgumentException();
    }

    long factor = (long) java.lang.Math.pow(10, places);
    value = value * factor;
    long tmp = (long) value;
    return (double) tmp / factor;
  }
}
