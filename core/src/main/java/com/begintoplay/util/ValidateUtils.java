package com.begintoplay.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * Created by manoj on 13/02/16.
 */
public class ValidateUtils {
  private static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
  private static Pattern pattern = Pattern.compile(EMAIL_PATTERN);

  /**
   * Validate the email address
   *
   * @param value The email address to validate
   * @return True if the value is a valid email address
   */
  public static boolean isValidEmail(String value) {
    return match(value);
  }

  public static boolean isValidPhone(String value) {
    return value.length() == 10;
  }

  private static boolean match(String value) {
    return StringUtils.isNotEmpty(value) && pattern.matcher(value).matches();
  }

  /**
   * Validate the URL
   *
   * @param value The URL to validate
   * @return True if the value is a valid URL
   */
  public static boolean isValidURL(String value) {
    try {
      new URL(value);
      return true;
    } catch (MalformedURLException ex) {
      return false;
    }
  }

  /**
   * Validate the URL without protocol
   *
   * @param value The URL to validate
   * @return True if the value is a valid URL
   */
  public static boolean isValidURLWithoutProtocol(String value) {
    return isValidURL("http://" + value);
  }

  public static boolean isValidDouble(String number) {
    try {
      Double.parseDouble(number);
      return true;
    } catch (Exception e) {
      return false;
    }
  }
}
