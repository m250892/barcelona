package com.begintoplay.util;

import android.content.Context;

import com.begintoplay.Logger;
import com.begintoplay.ui.BaseApplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileVersionHelper {

  public FileVersionHelper() {
  }

  public boolean isFileExist(String filename) {
    File file = BaseApplication.getContext().getFileStreamPath(filename);
    return file.exists();
  }

  public boolean writeDataInFile(String filename, String data) {
    FileOutputStream outputStream;
    try {
      outputStream = BaseApplication.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
      outputStream.write(data.getBytes());
      outputStream.close();
      return true;
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return false;
  }

  public String getDataFromFile(String filename) {
    Logger.print("Fetching data from internal storage for : " + filename);
    FileInputStream inputStream = null;
    try {
      inputStream = BaseApplication.getContext().openFileInput(filename);
      String result = readDataFromInputStream(inputStream);
      inputStream.close();
      return result;
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private String readDataFromInputStream(InputStream inputStream) {
    try {
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
      StringBuilder total = new StringBuilder();
      String line;
      while ((line = bufferedReader.readLine()) != null) {
        total.append(line);
      }
      return total.toString();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }


  public String getDefaultData(int fileId) {
    try {
      InputStream resourceReader = BaseApplication.getContext().getResources().openRawResource(fileId);
      String result = readDataFromInputStream(resourceReader);
      resourceReader.close();
      return result;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public String getLatestFile(String filename, int defaultId) {
    Logger.print(filename + " : data fetched called");
    if (isFileExist(filename)) {
      return getDataFromFile(filename);
    } else {
      Logger.print("Fetching default data for : " + filename);
      return getDefaultData(defaultId);
    }
  }
}
