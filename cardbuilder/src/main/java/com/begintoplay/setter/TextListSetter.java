package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.begintoplay.cardbuilder.CardBuilder;
import com.begintoplay.cardbuilder.R;
import com.begintoplay.holder.TextListHolder;

import java.util.List;

/**
 * Created by manoj on 08/01/17.
 */

public class TextListSetter extends BaseViewBuilder {

  protected TextListHolder buildViewHolder(ViewGroup parent) {
    View view = inflate(R.layout.child_textlist_view_layout, parent);
    return new TextListHolder(view);
  }

  protected void setView(TextListHolder holder, String text, int count) {
    holder.prefix.setText(String.valueOf(count) + ".");
    holder.description.setText(text);
  }

  public View buildAndGetView(List<String> data, ViewGroup parent) {
    LinearLayout linearLayout = new LinearLayout(CardBuilder.getContext());
    linearLayout.setOrientation(LinearLayout.VERTICAL);
    int count = 1;
    for (String text : data) {
      linearLayout.addView(buildAndGetView(text, count, linearLayout));
      count++;
    }
    return linearLayout;
  }

  public View buildAndGetView(String text, int count, ViewGroup parent) {
    TextListHolder baseCardHolder = buildViewHolder(parent);
    setView(baseCardHolder, text, count);
    return baseCardHolder.getView();
  }
}
