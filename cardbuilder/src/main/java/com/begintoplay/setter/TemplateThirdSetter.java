package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.begintoplay.cardbuilder.CardBuilder;
import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.KeyValuePair;
import com.begintoplay.cardtemplate.TemplateThirdModel;
import com.begintoplay.holder.TemplateThirdHolder;

/**
 * Created by manoj on 08/01/17.
 */

public class TemplateThirdSetter extends BaseCardSetter<TemplateThirdHolder, TemplateThirdModel> {

  @Override
  protected TemplateThirdHolder buildViewHolder(ViewGroup parent) {
    View view = inflate(R.layout.card_template_third_layout, parent);
    return new TemplateThirdHolder(view);
  }

  @Override
  protected void setView(TemplateThirdHolder holder, TemplateThirdModel model) {
    LinearLayout gridLayout = holder.gridContainer;
    int count = 0;
    LinearLayout linearLayout = null;
    for (KeyValuePair childData : model.getData()) {
      if (count == 0) {
        linearLayout = new LinearLayout(CardBuilder.getContext());
      }
      View childView = new KeyValueViewSetter().buildAndGetView(childData, linearLayout);
      LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) childView.getLayoutParams();
      lp.weight = 1;
      linearLayout.addView(childView, lp);
      if (count == 1) {
        count = 0;
        gridLayout.addView(linearLayout);
      } else {
        count++;
      }
    }
    //Check for odd element
    if (count > 0) {
      gridLayout.addView(linearLayout);
    }
  }

}
