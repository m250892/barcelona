package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.holder.CheckListHolder;

/**
 * Created by manoj on 08/01/17.
 */

public class ChecklistViewSetter extends BaseViewBuilder {

  protected CheckListHolder buildViewHolder(ViewGroup parent) {
    View view = inflate(R.layout.child_checklist_view_layout, parent);
    return new CheckListHolder(view);
  }

  protected void setView(CheckListHolder holder, String text) {
    holder.description.setText(text);
  }

  public View buildAndGetView(String text, ViewGroup parent) {
    CheckListHolder baseCardHolder = buildViewHolder(parent);
    setView(baseCardHolder, text);
    return baseCardHolder.getView();
  }
}
