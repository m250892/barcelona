package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.cardtemplate.BaseTemplateModel;
import com.begintoplay.event.CardEvent;
import com.begintoplay.holder.BaseCardHolder;
import com.begintoplay.managers.EventManager;

/**
 * Created by manoj on 07/01/17.
 */

public abstract class BaseCardSetter<T extends BaseCardHolder, M extends BaseTemplateModel> extends BaseViewBuilder {

  public View buildAndGetView(M model, ViewGroup parent) {
    T baseCardHolder = buildViewHolder(parent);
    setView(baseCardHolder, model);
    return baseCardHolder.getView();
  }

  protected void sendEvent(BaseTemplateModel model) {
    EventManager.getDefaultEventBus().post(new CardEvent(model));
  }
  protected abstract T buildViewHolder(ViewGroup parent);

  protected abstract void setView(T holder, M model);
}
