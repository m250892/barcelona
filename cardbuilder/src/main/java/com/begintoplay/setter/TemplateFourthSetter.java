package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.adapter.FourthChildPagerAdapter;
import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.TemplateFourthModel;
import com.begintoplay.holder.TemplateFourthHolder;

/**
 * Created by manoj on 08/01/17.
 */

public class TemplateFourthSetter extends BaseCardSetter<TemplateFourthHolder, TemplateFourthModel> {

  @Override
  protected TemplateFourthHolder buildViewHolder(ViewGroup parent) {
    View view = inflate(R.layout.card_template_fourth_layout, parent);
    return new TemplateFourthHolder(view);
  }

  @Override
  protected void setView(TemplateFourthHolder holder, final TemplateFourthModel model) {
    FourthChildPagerAdapter adapter = new FourthChildPagerAdapter(model.getData());
    holder.viewPager.setAdapter(adapter);
    holder.tabStrip.setViewPager(holder.viewPager);
    holder.more.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        sendEvent(model);
      }
    });
  }
}
