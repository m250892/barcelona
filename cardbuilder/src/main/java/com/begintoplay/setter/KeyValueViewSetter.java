package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.KeyValuePair;
import com.begintoplay.holder.KeyValueViewHolder;

/**
 * Created by manoj on 08/01/17.
 */

public class KeyValueViewSetter extends BaseViewBuilder {

  protected KeyValueViewHolder buildViewHolder(ViewGroup parent) {
    View view = inflate(R.layout.child_key_value_view_layout, parent);
    return new KeyValueViewHolder(view);
  }

  protected void setView(KeyValueViewHolder holder, KeyValuePair childData) {
    holder.heading.setText(childData.getKey());
    holder.description.setText(childData.getValue());
  }

  public View buildAndGetView(KeyValuePair model, ViewGroup parent) {
    KeyValueViewHolder baseCardHolder = buildViewHolder(parent);
    setView(baseCardHolder, model);
    return baseCardHolder.getView();
  }

}
