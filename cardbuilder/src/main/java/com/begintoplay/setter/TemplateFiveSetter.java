package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.TemplateFiveModel;
import com.begintoplay.holder.TemplateTwoHolder;

/**
 * Created by manoj on 08/01/17.
 */

public class TemplateFiveSetter extends BaseCardSetter<TemplateTwoHolder, TemplateFiveModel> {

  @Override
  protected TemplateTwoHolder buildViewHolder(ViewGroup parent) {
    View view = inflate(R.layout.card_template_two_layout, parent);
    return new TemplateTwoHolder(view);
  }

  @Override
  protected void setView(TemplateTwoHolder holder, final TemplateFiveModel model) {
    holder.title.setText(model.getTitle());
    holder.title.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        sendEvent(model);
      }
    });
  }
}
