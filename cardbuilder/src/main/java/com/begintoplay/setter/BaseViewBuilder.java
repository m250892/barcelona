package com.begintoplay.setter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.cardbuilder.CardBuilder;

/**
 * Created by manoj on 08/01/17.
 */

public class BaseViewBuilder {

  private LayoutInflater getInflater() {
    return LayoutInflater.from(CardBuilder.getContext());
  }

  protected View inflate(int layoutId, ViewGroup parent) {
    return getInflater().inflate(layoutId, parent, false);
  }
}
