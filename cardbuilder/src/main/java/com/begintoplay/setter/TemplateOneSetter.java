package com.begintoplay.setter;

import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.TemplateOneModel;
import com.begintoplay.holder.TemplateOneHolder;

/**
 * Created by manoj on 07/01/17.
 */

public class TemplateOneSetter extends BaseCardSetter<TemplateOneHolder, TemplateOneModel> {

  @Override
  protected TemplateOneHolder buildViewHolder(ViewGroup parent) {
    View view = inflate(R.layout.card_template_one_layout, parent);
    return new TemplateOneHolder(view);
  }

  @Override
  protected void setView(TemplateOneHolder holder, final TemplateOneModel model) {
    holder.heading.setText(model.getTitle());
    holder.description.setText(model.getDescription());
    holder.more.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        sendEvent(model);
      }
    });
  }

}
