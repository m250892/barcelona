package com.begintoplay.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.cardtemplate.KeyContentModel;
import com.begintoplay.setter.TextListSetter;

import java.util.List;

/**
 * Created by manoj on 08/01/17.
 */

public class FourthChildPagerAdapter extends PagerAdapter {

  final List<KeyContentModel> data;

  public FourthChildPagerAdapter(List<KeyContentModel> data) {
    super();
    this.data = data;
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view == object;
  }

  @Override
  public View instantiateItem(ViewGroup container, int position) {
    KeyContentModel contentModel = data.get(position);
    View view = new TextListSetter().buildAndGetView(contentModel.getContent(), container);
    container.addView(view);
    return view;
  }

  @Override
  public int getCount() {
    return data.size();
  }

  @Override
  public CharSequence getPageTitle(int position) {
    return data.get(position).getTitle();
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    if (object instanceof View) {
      container.removeView((View) object);
    }
  }
}
