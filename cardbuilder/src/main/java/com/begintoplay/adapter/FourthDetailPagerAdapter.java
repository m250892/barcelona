package com.begintoplay.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.begintoplay.cardtemplate.KeyContentModel;
import com.begintoplay.fragments.ChildFourthFragment;

import java.util.List;

/**
 * Created by manoj on 08/01/17.
 */

public class FourthDetailPagerAdapter extends FragmentStatePagerAdapter {

  private final List<KeyContentModel> data;

  public FourthDetailPagerAdapter(FragmentManager fm, List<KeyContentModel> data) {
    super(fm);
    this.data = data;
  }

  @Override
  public Fragment getItem(int position) {
    KeyContentModel model = data.get(position);
    return ChildFourthFragment.newInstance(model);
  }

  @Override
  public CharSequence getPageTitle(int position) {
    return data.get(position).getTitle();
  }

  @Override
  public int getCount() {
    return data.size();
  }
}
