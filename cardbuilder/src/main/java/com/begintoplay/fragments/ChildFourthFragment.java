package com.begintoplay.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.KeyContentModel;
import com.begintoplay.setter.TextListSetter;
import com.begintoplay.ui.BaseFragment;

public class ChildFourthFragment extends BaseFragment {

  private static final String BUNDLE_INPUT_DATA = "bundle_input_data";

  private KeyContentModel keyContentModel;

  public ChildFourthFragment() {
    // Required empty public constructor
  }

  public static ChildFourthFragment newInstance(KeyContentModel data) {
    ChildFourthFragment childFourthFragment = new ChildFourthFragment();
    Bundle bundle = new Bundle();
    bundle.putSerializable(BUNDLE_INPUT_DATA, data);
    childFourthFragment.setArguments(bundle);
    return childFourthFragment;
  }


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    keyContentModel = (KeyContentModel) getArguments().getSerializable(BUNDLE_INPUT_DATA);
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_child_fouth;
  }

  @Override
  protected void initViews(View baseView) {
    LinearLayout linearLayout = (LinearLayout) baseView.findViewById(R.id.container);
    int count = 1;
    for (String data : keyContentModel.getContent()) {
      linearLayout.addView(new TextListSetter().buildAndGetView(data, count, linearLayout));
      count++;
    }
  }
}
