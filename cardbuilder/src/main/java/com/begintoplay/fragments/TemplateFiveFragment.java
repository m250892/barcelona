package com.begintoplay.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.TemplateFiveModel;
import com.begintoplay.setter.ChecklistViewSetter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TemplateFiveFragment extends BaseCardDetailFragment<TemplateFiveModel> {

  LinearLayout container;
  TemplateFiveModel fiveModel;

  public TemplateFiveFragment() {
    // Required empty public constructor
  }

  public static TemplateFiveFragment newInstance(TemplateFiveModel templateFiveModel) {
    TemplateFiveFragment templateFiveFragment = new TemplateFiveFragment();
    Bundle args = new Bundle();
    args.putSerializable(BUNDLE_TEMPLATE_MODEL_DATA, templateFiveModel);
    templateFiveFragment.setArguments(args);
    return templateFiveFragment;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_template_five;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    fiveModel = (TemplateFiveModel) getArguments().getSerializable(BUNDLE_TEMPLATE_MODEL_DATA);
  }

  @Override
  protected void initViews(View baseView) {
    Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
    setToolbar(toolbar);
    setToolbarTitle(fiveModel.getTitle());

    container = (LinearLayout) baseView.findViewById(R.id.container);
    for (String childModel : fiveModel.getData()) {
      container.addView(new ChecklistViewSetter().buildAndGetView(childModel, container));
    }
  }
}
