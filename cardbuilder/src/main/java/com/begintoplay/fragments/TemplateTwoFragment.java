package com.begintoplay.fragments;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.KeyValuePair;
import com.begintoplay.cardtemplate.TemplateTwoModel;
import com.begintoplay.setter.KeyValueViewSetter;

public class TemplateTwoFragment extends BaseCardDetailFragment {

  LinearLayout container;
  TemplateTwoModel twoModel;

  public TemplateTwoFragment() {
    // Required empty public constructor
  }

  public static TemplateTwoFragment newInstance(TemplateTwoModel templateTwoModel) {
    TemplateTwoFragment templateTwoFragment = new TemplateTwoFragment();
    Bundle args = new Bundle();
    args.putSerializable(BUNDLE_TEMPLATE_MODEL_DATA, templateTwoModel);
    templateTwoFragment.setArguments(args);
    return templateTwoFragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    twoModel = (TemplateTwoModel) getArguments().getSerializable(BUNDLE_TEMPLATE_MODEL_DATA);
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_template_two;
  }

  @Override
  protected void initViews(View baseView) {
    Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
    setToolbar(toolbar);
    setToolbarTitle(twoModel.getTitle());

    container = (LinearLayout) baseView.findViewById(R.id.container);
    for (KeyValuePair childModel : twoModel.getData()) {
      container.addView(new KeyValueViewSetter().buildAndGetView(childModel, container));
    }
  }
}
