package com.begintoplay.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.View;

import com.begintoplay.adapter.FourthDetailPagerAdapter;
import com.begintoplay.cardtemplate.TemplateFourthModel;
import com.begintoplay.ui.BaseListHolderFragment;

import static com.begintoplay.fragments.BaseCardDetailFragment.BUNDLE_TEMPLATE_MODEL_DATA;

/**
 * A simple {@link Fragment} subclass.
 */
public class TemplateFourthFragment extends BaseListHolderFragment {

  TemplateFourthModel fourthModel;

  public TemplateFourthFragment() {
    // Required empty public constructor
  }

  public static TemplateFourthFragment newInstance(TemplateFourthModel fourthModel) {
    TemplateFourthFragment templateFourthFragment = new TemplateFourthFragment();
    Bundle args = new Bundle();
    args.putSerializable(BUNDLE_TEMPLATE_MODEL_DATA, fourthModel);
    templateFourthFragment.setArguments(args);
    return templateFourthFragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    fourthModel = (TemplateFourthModel) getArguments().getSerializable(BUNDLE_TEMPLATE_MODEL_DATA);
  }

  @Override
  protected void initViews(View baseView) {
    super.initViews(baseView);
    tabLayout.setTabMode(TabLayout.MODE_FIXED);
    tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
  }

  @Override
  protected void setViewPagerAdapter() {
    FourthDetailPagerAdapter pagerAdapter = new FourthDetailPagerAdapter(getFragmentManager(), fourthModel.getData());
    viewPager.setAdapter(pagerAdapter);
  }
}
