package com.begintoplay.fragments;

import com.begintoplay.cardtemplate.BaseTemplateModel;
import com.begintoplay.ui.BaseFragment;

/**
 * Created by manoj on 08/01/17.
 */

public abstract class BaseCardDetailFragment<T extends BaseTemplateModel> extends BaseFragment {
  static final String BUNDLE_TEMPLATE_MODEL_DATA = "bundle_template_model_data";
}
