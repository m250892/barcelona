package com.begintoplay.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.begintoplay.cardbuilder.R;
import com.begintoplay.cardtemplate.TemplateOneModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class TemplateOneFragment extends BaseCardDetailFragment<TemplateOneModel> {

  private TemplateOneModel oneModel;
  TextView descriptionView;

  public TemplateOneFragment() {
    // Required empty public constructor
  }

  public static TemplateOneFragment newInstance(TemplateOneModel templateOneModel) {
    TemplateOneFragment templateOneFragment = new TemplateOneFragment();
    Bundle args = new Bundle();
    args.putSerializable(BUNDLE_TEMPLATE_MODEL_DATA, templateOneModel);
    templateOneFragment.setArguments(args);
    return templateOneFragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    oneModel = (TemplateOneModel) getArguments().getSerializable(BUNDLE_TEMPLATE_MODEL_DATA);
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_template_one;
  }

  @Override
  protected void initViews(View baseView) {
    Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
    setToolbar(toolbar);
    setToolbarTitle(oneModel.getTitle());

    descriptionView = (TextView) baseView.findViewById(R.id.description);
    descriptionView.setText(oneModel.getDescription());
  }
}
