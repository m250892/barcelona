package com.begintoplay.event;

import com.begintoplay.cardtemplate.BaseTemplateModel;

/**
 * Created by manoj on 08/01/17.
 */

public class CardEvent {
  private BaseTemplateModel model;

  public CardEvent(BaseTemplateModel model) {
    this.model = model;
  }

  public BaseTemplateModel getModel() {
    return model;
  }
}
