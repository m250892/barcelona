package com.begintoplay.holder;

import android.view.View;
import android.widget.TextView;

import com.begintoplay.cardbuilder.R;

/**
 * Created by manoj on 07/01/17.
 */

public class TemplateOneHolder extends BaseCardHolder {
  public TextView heading;
  public TextView description;
  public View more;

  public TemplateOneHolder(View rootView) {
    super(rootView);
    heading = (TextView) rootView.findViewById(R.id.heading);
    description = (TextView) rootView.findViewById(R.id.description);
    more = rootView.findViewById(R.id.more);
  }
}
