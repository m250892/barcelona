package com.begintoplay.holder;

import android.view.View;
import android.widget.TextView;

import com.begintoplay.cardbuilder.R;

/**
 * Created by manoj on 08/01/17.
 */

public class CheckListHolder extends BaseCardHolder {

  public TextView description;

  public CheckListHolder(View rootView) {
    super(rootView);
    description = (TextView) rootView.findViewById(R.id.description);
  }
}
