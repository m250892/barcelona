package com.begintoplay.holder;

import android.view.View;
import android.widget.TextView;

import com.begintoplay.cardbuilder.R;

/**
 * Created by manoj on 08/01/17.
 */

public class KeyValueViewHolder extends BaseCardHolder {
  public TextView heading;
  public TextView description;

  public KeyValueViewHolder(View rootView) {
    super(rootView);
    heading = (TextView) rootView.findViewById(R.id.heading);
    description = (TextView) rootView.findViewById(R.id.description);
  }
}
