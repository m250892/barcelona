package com.begintoplay.holder;

import android.view.View;

/**
 * Created by manoj on 07/01/17.
 */

public abstract class BaseCardHolder {
  private final View rootView;

  public BaseCardHolder(View rootView) {
    this.rootView = rootView;
  }

  public View getView() {
    return rootView;
  }
}
