package com.begintoplay.holder;

import android.view.View;
import android.widget.TextView;

import com.begintoplay.cardbuilder.R;

/**
 * Created by manoj on 08/01/17.
 */

public class TemplateTwoHolder extends BaseCardHolder {
  public TextView title;

  public TemplateTwoHolder(View rootView) {
    super(rootView);
    title = (TextView) rootView.findViewById(R.id.title);
  }
}
