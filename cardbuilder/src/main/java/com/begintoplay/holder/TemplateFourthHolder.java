package com.begintoplay.holder;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.astuetz.PagerSlidingTabStrip;
import com.begintoplay.cardbuilder.R;

/**
 * Created by manoj on 08/01/17.
 */

public class TemplateFourthHolder extends BaseCardHolder {
  public View more;
  public PagerSlidingTabStrip tabStrip;
  public ViewPager viewPager;

  public TemplateFourthHolder(View rootView) {
    super(rootView);
    tabStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
    viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
    more = rootView.findViewById(R.id.more);
  }
}
