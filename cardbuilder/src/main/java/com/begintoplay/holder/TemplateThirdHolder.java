package com.begintoplay.holder;

import android.view.View;
import android.widget.LinearLayout;

import com.begintoplay.cardbuilder.R;

/**
 * Created by manoj on 08/01/17.
 */

public class TemplateThirdHolder extends BaseCardHolder {

  public LinearLayout gridContainer;
  public TemplateThirdHolder(View rootView) {
    super(rootView);
    gridContainer = (LinearLayout) rootView.findViewById(R.id.grid_container);
  }
}
