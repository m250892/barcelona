package com.begintoplay.holder;

import android.view.View;
import android.widget.TextView;

import com.begintoplay.cardbuilder.R;

/**
 * Created by manoj on 08/01/17.
 */

public class TextListHolder extends CheckListHolder {

  public TextView prefix;
  public TextListHolder(View rootView) {
    super(rootView);
    prefix = (TextView) rootView.findViewById(R.id.prefix);
  }
}
