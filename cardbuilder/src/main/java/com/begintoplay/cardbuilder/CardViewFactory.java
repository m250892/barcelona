package com.begintoplay.cardbuilder;

import android.view.View;
import android.view.ViewGroup;

import com.begintoplay.Logger;
import com.begintoplay.cardtemplate.BaseTemplateModel;
import com.begintoplay.cardtemplate.TemplateCardType;
import com.begintoplay.setter.BaseCardSetter;
import com.begintoplay.setter.TemplateFiveSetter;
import com.begintoplay.setter.TemplateFourthSetter;
import com.begintoplay.setter.TemplateOneSetter;
import com.begintoplay.setter.TemplateThirdSetter;
import com.begintoplay.setter.TemplateTwoSetter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by manoj on 07/01/17.
 */

public class CardViewFactory {

  static Map<TemplateCardType, ? extends BaseCardSetter> typeSetterMap =
   new HashMap<TemplateCardType, BaseCardSetter>() {
     {
       put(TemplateCardType.TEMPLATE_CARD_ONE, new TemplateOneSetter());
       put(TemplateCardType.TEMPLATE_CARD_TWO, new TemplateTwoSetter());
       put(TemplateCardType.TEMPLATE_CARD_THIRD, new TemplateThirdSetter());
       put(TemplateCardType.TEMPLATE_CARD_FOURTH, new TemplateFourthSetter());
       put(TemplateCardType.TEMPLATE_CARD_FIVE, new TemplateFiveSetter());
     }
   };

  public static View getView(BaseTemplateModel dataModel, ViewGroup parent) {
    BaseCardSetter cardSetter = typeSetterMap.get(dataModel.getCardType());
    if (cardSetter == null) {
      Logger.print("CardSetter not defined  for dataType : " + dataModel.getDataType());
      return null;
    }
    return cardSetter.buildAndGetView(dataModel, parent);
  }
}
