package com.begintoplay.cardbuilder;

import com.begintoplay.cardtemplate.BaseTemplateModel;
import com.begintoplay.cardtemplate.TemplateFiveModel;
import com.begintoplay.cardtemplate.TemplateFourthModel;
import com.begintoplay.cardtemplate.TemplateOneModel;
import com.begintoplay.cardtemplate.TemplateTwoModel;
import com.begintoplay.fragments.TemplateFiveFragment;
import com.begintoplay.fragments.TemplateFourthFragment;
import com.begintoplay.fragments.TemplateOneFragment;
import com.begintoplay.fragments.TemplateTwoFragment;
import com.begintoplay.ui.BaseFragment;

/**
 * Created by manoj on 08/01/17.
 */

public class CardFragmentFactory {

  public static BaseFragment getFragment(BaseTemplateModel baseTemplateModel) {
    switch (baseTemplateModel.getCardType()) {
      case TEMPLATE_CARD_ONE:
        return TemplateOneFragment.newInstance((TemplateOneModel) baseTemplateModel);
      case TEMPLATE_CARD_TWO:
        return TemplateTwoFragment.newInstance((TemplateTwoModel) baseTemplateModel);
      case TEMPLATE_CARD_FOURTH:
        return TemplateFourthFragment.newInstance((TemplateFourthModel) baseTemplateModel);
      case TEMPLATE_CARD_FIVE:
        return TemplateFiveFragment.newInstance((TemplateFiveModel) baseTemplateModel);
    }
    return null;
  }
}
