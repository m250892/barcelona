package com.begintoplay.cardbuilder;

import android.content.Context;

/**
 * Created by manoj on 08/01/17.
 */

public class CardBuilder {
  private static Context context;

  private CardBuilder() {
  }

  public static void initialize(Context ct) {
    context = ct;
  }

  public static Context getContext() {
    if (context == null) {
      throw new IllegalStateException("getContext called without initialization");
    }
    return context;
  }
}
