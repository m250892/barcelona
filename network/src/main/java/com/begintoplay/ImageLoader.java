package com.begintoplay;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.begintoplay.util.NLogger;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by manoj on 19/02/16.
 */

public class ImageLoader {
  private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.RGB_565;
  private static final MemoryPolicy MEMORY_POLICY = MemoryPolicy.NO_CACHE;
  private static final boolean USE_MEMORY_CACHE = true;
  private static final float LOW_MEMORY_THRESHOLD_PERCENTAGE = 5;
  private AtomicInteger networkSamplerCount;
  public static final boolean ENABLE_FACEBOOK_CONNECTION_SAMPLING = false;

  private Picasso picasso;
  private Context context;
  private static ImageLoader instance;

  private ImageLoader(Context context) {
    this.context = context;
    networkSamplerCount = new AtomicInteger();
    picasso = getPicassoImageLoader();
  }

  public static synchronized ImageLoader getInstance() {
    if (instance == null) {
      instance = new ImageLoader(Rest.getContext());
    }
    return instance;
  }

  private Picasso getPicassoImageLoader() {
    OkHttpClient picassoClient = new OkHttpClient();
    picassoClient.interceptors().add(new Interceptor() {
      @Override
      public Response intercept(Chain chain) throws IOException {
        Request newRequest = chain.request().newBuilder().addHeader("Accept", "image/webp")
                              .build();
        return chain.proceed(newRequest);
      }
    });
    return new Picasso.Builder(context)
            .downloader(new OkHttpDownloader(picassoClient)).build();
  }


  public void loadImage(String url, ImageView imageView) {
    loadImage(url, imageView, null, null);
  }

  public void loadImage(String url, ImageView imageView, Drawable backupDrawable) {
    loadImage(url, imageView, backupDrawable, backupDrawable);
  }


  public void loadImage(String url, ImageView imageView, Drawable drawableWhileLoading,
                        Drawable drawableOnFailure) {
    if (isAdequateMemoryAvailable()) {
      if (imageView != null && !TextUtils.isEmpty(url)) {
        RequestCreator request = picasso.load(url);
        if (drawableWhileLoading != null) {
          request.placeholder(drawableWhileLoading);
        }
        if (drawableOnFailure != null) {
          request.error(drawableOnFailure);
        }
        request.config(BITMAP_CONFIG);
        if (!USE_MEMORY_CACHE) {
          request.memoryPolicy(MEMORY_POLICY);
        }
        request.into(imageView);
        return;
      }
    }
    if (imageView != null && drawableOnFailure != null) {
      imageView.setImageDrawable(drawableOnFailure);
    }
  }


  public static void setPic(ImageView mImageView, String mCurrentPhotoPath) {
    // Get the dimensions of the View
    int targetW = mImageView.getMeasuredWidth();
    int targetH = mImageView.getMeasuredHeight();
    setPic(mImageView, mCurrentPhotoPath, targetW, targetH);
  }


  public static void setPic(ImageView mImageView, String mCurrentPhotoPath, int width, int height) {
    // Get the dimensions of the bitmap
    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
    bmOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

    bmOptions.inSampleSize = calculateInSampleSize(bmOptions, width, height);
    bmOptions.inJustDecodeBounds = false;
    mImageView.setImageBitmap(BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions));
  }

  public static int calculateInSampleSize(
                                          BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

      final int halfHeight = height / 2;
      final int halfWidth = width / 2;

      // Calculate the largest inSampleSize value that is a power of 2 and keeps both
      // height and width larger than the requested height and width.
      while ((halfHeight / inSampleSize) > reqHeight
              || (halfWidth / inSampleSize) > reqWidth) {
        inSampleSize *= 2;
      }
    }

    return inSampleSize;
  }


  private boolean isAdequateMemoryAvailable() {

    boolean isAdequateMemoryAvailable = true;
    // Get app memory info
    long total = Runtime.getRuntime().maxMemory();
    long used = Runtime.getRuntime().totalMemory();
    float percentAvailable = 100f * (1f - ((float) used / total));
    if (percentAvailable <= LOW_MEMORY_THRESHOLD_PERCENTAGE) {
      NLogger.print("LOW MEMORY, ONLY " + percentAvailable + "% AVAILABLE!");
      isAdequateMemoryAvailable = false;
      handleLowMemory();
    }

    return isAdequateMemoryAvailable;
  }

  private void handleLowMemory() {
    // handle low memory
    NLogger.print("HANDLING LOW MEMORY");
    System.gc();
  }


}
