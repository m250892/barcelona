package com.begintoplay.callback;

public interface RequestCallback<T> {
  void onRequestSuccess(T t);

  void onRequestError(RequestError error);
}