package com.begintoplay;

import com.begintoplay.detailpage.EventDetailPageDTO;
import com.begintoplay.detailpage.EventDetailPageResponse;
import com.begintoplay.eventlist.EventCardDto;
import com.begintoplay.eventlist.EventListPageResponse;
import com.begintoplay.network.R;
import com.begintoplay.util.FileUtil;
import com.begintoplay.util.GsonUtil;

import java.util.List;

import static com.begintoplay.Rest.context;

/**
 * Created by manoj on 07/01/17.
 */

public class ImNetwork {

  public static EventDetailPageDTO getEventDetail() {
    String data = FileUtil.loadData(context, R.raw.event);
    EventDetailPageResponse page = GsonUtil.fromJson(data, EventDetailPageResponse.class);
    return page;
  }

  public static List<EventCardDto> getEventCardList() {
    String data = FileUtil.loadData(context, R.raw.event_list);
    EventListPageResponse page = GsonUtil.fromJson(data, EventListPageResponse.class);
    return page.getEventCardList();
  }
}
