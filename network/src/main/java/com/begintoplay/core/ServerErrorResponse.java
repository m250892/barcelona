package com.begintoplay.core;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServerErrorResponse {

  @SerializedName("error")
  private Object message;

  public String getMessage() {
    if (message == null) {
      return "Couldn't fetch message from responseObject";
    }
    if (message instanceof String) {
      return message.toString();
    } else if (message instanceof List) {
      return message.toString();
    } else {
      return "Couldn't fetch message from responseObject";
    }
  }

  public void setMessage(Object messageObject) {
    this.message = messageObject;
  }

}