package com.begintoplay.core;

import android.net.Uri;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.begintoplay.Rest;
import com.begintoplay.callback.RequestCallback;
import com.begintoplay.callback.RequestError;
import com.begintoplay.util.Constants;
import com.begintoplay.util.GsonUtil;
import com.begintoplay.util.NLogger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class NetworkManager {
  public enum RESULT {
    JSONOBJECT,
    JSONARRAY
  }

  private final NetworkHelper networkHelper;
  private final String pathUrl;
  private final int method;
  private final TypeToken<?> classTarget;
  private final RESULT resultType;
  private final HashMap<String, Object> bodyRequest;
  private final HashMap<String, String> headers;
  private final JSONObject bodyJsonObj;

  public NetworkManager(Builder builder) {
    this.networkHelper = NetworkHelper.getInstance(Rest.getContext());
    this.pathUrl = builder.pathUrl;
    this.method = builder.method;
    this.classTarget = builder.targetType;
    this.resultType = builder.resultType;
    this.bodyRequest = builder.bodyRequest;
    this.headers = builder.headers;
    this.bodyJsonObj = builder.bodyJsonObj;
  }


  private RequestError getGsonParsingError(Exception e) {
    e.printStackTrace();
    return new RequestError.Builder()
            .setErrorCode(RequestError.REQUEST_RESPONSE_GSON_PARSING_FAILD)
            .setErrorMessage(e.getMessage())
            .build();
  }

  private void fromJsonObject(final HashMap<String, String> headers, JSONObject bodyRequest, String requestTag, final RequestCallback requestCallback) {
    JsonObjectRequest request = new JsonObjectRequest(method, pathUrl, bodyRequest, new Response.Listener<JSONObject>() {
      @Override
      public void onResponse(JSONObject jsonObject) {
        NLogger.print("Success : " + jsonObject);
        try {
          Object t = new Gson().fromJson(jsonObject.toString(), classTarget.getType());
          Thread.sleep(Constants.API_RESPONSE_DELAY_BY_SEC * 1000);
          if (requestCallback != null)
            requestCallback.onRequestSuccess(t);
        } catch (Exception e) {
          if (requestCallback != null)
            requestCallback.onRequestError(getGsonParsingError(e));
        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        NLogger.print("Error : " + error);
        if (requestCallback != null) {
          NetworkResponse response = error.networkResponse;
          if (response != null) {
            String serverMsg = new String(response.data);
            ServerErrorResponse serverErrorResponse = GsonUtil.fromJson(serverMsg, ServerErrorResponse.class);
            RequestError requestError = new RequestError.Builder()
                                         .setErrorCode(response.statusCode)
                                         .setErrorMessage(serverErrorResponse.getMessage())
                                         .build();
            requestCallback.onRequestError(requestError);
          }
        }
      }
    }) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
      }
    };

    networkHelper.addToRequestQueue(request, requestTag);
  }


  private void fromJsonArray(final Map<String, String> headers, String requestTag, final RequestCallback requestCallback) {
    JsonArrayRequest request = new JsonArrayRequest(pathUrl, new Response.Listener<JSONArray>() {
      @Override
      public void onResponse(JSONArray jsonArray) {
        Object t = new Gson().fromJson(jsonArray.toString(), classTarget.getType());
        if (requestCallback != null)
          requestCallback.onRequestSuccess(t);
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        if (requestCallback != null) {
          NetworkResponse response = error.networkResponse;
          RequestError requestError = null;
          if (response != null) {
            requestError = new RequestError.Builder(response).build();
          } else {
            requestError = new RequestError.Builder().setErrorMessage(error.toString()).build();
          }
          requestCallback.onRequestError(requestError);
        }
      }
    }) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
      }
    };

    networkHelper.addToRequestQueue(request, requestTag);
  }

  public void withCallback(RequestCallback callback) {

    String requestTag = Rest.DEFAULT_TAG;

    if (resultType == null) {
      throw new IllegalArgumentException("result type must not be null.");
    }

    if (classTarget == null) {
      throw new IllegalArgumentException("class target must not be null.");
    }

    if (pathUrl == null) {
      throw new IllegalArgumentException("path url must not be null.");
    }

    switch (resultType) {
      case JSONARRAY:
        fromJsonArray(headers, requestTag, callback);
        break;
      case JSONOBJECT:
        if (method == Request.Method.POST)
          if (bodyJsonObj == null)
            throw new IllegalArgumentException("body request url must not be null.");

        fromJsonObject(headers, bodyJsonObj, requestTag, callback);
        break;
      default:
        throw new IllegalArgumentException("response type not found");
    }
  }

  public static class Builder implements INetworkManagerBuilder {
    private String pathUrl;
    private int method;
    private RESULT resultType;
    private TypeToken<?> targetType;
    private HashMap<String, Object> bodyRequest;
    private HashMap<String, String> headers;
    private JSONObject bodyJsonObj;

    public Builder setMethod(int method) {
      this.method = method;
      return this;
    }

    public Builder setBodyJsonObj(JSONObject jsonObj) {
      this.bodyJsonObj = jsonObj;
      return this;
    }

    public Builder setBodyRequest(HashMap<String, Object> bodyRequest) {
      this.bodyRequest = bodyRequest;
      return this;
    }

    public Builder setHeaders(HashMap<String, String> headers) {
      this.headers = headers;
      return this;
    }

    @Override
    public INetworkManagerBuilder load(String pathUrl) {
      pathUrl = addExtraParams(pathUrl);
      this.pathUrl = pathUrl;
      NLogger.print("Creating Network request for URL : " + pathUrl);
      return this;
    }

    private String addExtraParams(String pathUrl) {
      Uri.Builder builder = Uri.parse(pathUrl).buildUpon();
      builder.appendQueryParameter("source", "mobile");
      String url = builder.build().toString();
      return url;
    }

    @Override
    public NetworkManager as(Class classTarget) {
      this.targetType = TypeToken.get(classTarget);
      this.resultType = RESULT.JSONOBJECT;
      return new NetworkManager(this);
    }

    @Override
    public NetworkManager as(TypeToken typeToken) {
      this.targetType = typeToken;
      this.resultType = typeToken.getRawType().equals(List.class) ? RESULT.JSONARRAY : RESULT.JSONOBJECT;

      return new NetworkManager(this);
    }

  }

  public interface INetworkManagerBuilder {

    INetworkManagerBuilder load(String pathUrl);

    NetworkManager as(Class classTarget);

    NetworkManager as(TypeToken typeToken);
  }
}