package com.begintoplay.util;

/**
 * Created by manoj on 07/01/17.
 */

public class Constants {
  public static final int API_RESPONSE_DELAY_BY_SEC = 0;
  public static String TAG = "network";
  public static boolean ENABLE_LOG = false;
  public static final int DISK_CACHE_BYTES = 1024 * 1024;// 1MB cap
}
