package com.begintoplay.util;

import android.util.Log;

/**
 * Created by manoj on 07/01/17.
 */

public class NLogger {

  public static void print(String message) {
    if (Constants.ENABLE_LOG) {
      Log.d(Constants.TAG, message);
    }
  }
}
