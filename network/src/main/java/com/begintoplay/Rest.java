package com.begintoplay;

import android.content.Context;

import com.begintoplay.core.NetworkCreator;
import com.begintoplay.core.NetworkHelper;
import com.begintoplay.core.NetworkManager;

import org.json.JSONObject;

public class Rest {
  public static final String DEFAULT_TAG = "network_rest";
  private static Rest singleton = null;
  public static Context context;

  private Rest(Context context) {
    this.context = context;
  }

  public synchronized static Rest init(Context context) {
    if (singleton == null) {
      singleton = new Rest(context);
    }
    return singleton;
  }

  private NetworkCreator createRequest() {
    return new NetworkCreator();
  }

  public static Context getContext() {
    return context;
  }

  public static void cancelRequests() {
    if (singleton == null) {
      throw new IllegalStateException("QuickUtils not instantiated yet.");
    }
    NetworkHelper.getInstance(context).cancelPendingRequests(Rest.DEFAULT_TAG);
  }

  public static NetworkManager.INetworkManagerBuilder GET(String url) {
    return connect().GET().load(url);
  }

  public static NetworkManager.INetworkManagerBuilder POST(String url, JSONObject data) {
    return connect().POST(data).load(url);
  }

  public synchronized static NetworkCreator connect() {
    if (singleton == null) {
      throw new IllegalStateException("QuickUtils not instantiated yet.");
    }
    return singleton.createRequest();
  }
}
