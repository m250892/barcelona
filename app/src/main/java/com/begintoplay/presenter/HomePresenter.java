package com.begintoplay.presenter;

import com.begintoplay.storage.DataStorage;
import com.begintoplay.view.HomeView;

/**
 * Created by manoj on 07/01/17.
 */

public class HomePresenter {
  private final HomeView view;
  private final DataStorage storage;

  public HomePresenter(HomeView view, DataStorage storage) {
    this.view = view;
    this.storage = storage;
  }

  public void onEventClick(String eventId) {
    view.showDetailPage(storage.getEventDetail(eventId));
  }
}
