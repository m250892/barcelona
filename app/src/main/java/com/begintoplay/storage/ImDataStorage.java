package com.begintoplay.storage;

import com.begintoplay.ImNetwork;
import com.begintoplay.detailpage.EventDetailPageDTO;

/**
 * Created by manoj on 07/01/17.
 */

public class ImDataStorage implements DataStorage {

  @Override
  public EventDetailPageDTO getEventDetail(String eventId) {
    return ImNetwork.getEventDetail();
  }
}
