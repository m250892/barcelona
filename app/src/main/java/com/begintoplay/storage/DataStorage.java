package com.begintoplay.storage;

import com.begintoplay.detailpage.EventDetailPageDTO;

/**
 * Created by manoj on 07/01/17.
 */

public interface DataStorage {
  EventDetailPageDTO getEventDetail(String eventId);
}
