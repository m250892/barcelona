package com.begintoplay;

import com.begintoplay.cardbuilder.CardBuilder;
import com.begintoplay.ui.BaseApplication;

/**
 * Created by manoj on 07/01/17.
 */

public class MainApplication extends BaseApplication {

  @Override
  public void onCreate() {
    super.onCreate();

    //Initializing CardBuilder Module
    CardBuilder.initialize(this);
  }
}
