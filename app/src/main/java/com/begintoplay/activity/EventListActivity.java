package com.begintoplay.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.begintoplay.ImNetwork;
import com.begintoplay.adapter.EventListAdapter;
import com.begintoplay.begintoplay.R;
import com.begintoplay.eventlist.EventCardDto;

import java.util.List;

public class EventListActivity extends AppCompatActivity {

  RecyclerView recyclerView;
  private List<EventCardDto> eventCards;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_event_list);
    recyclerView = (RecyclerView) findViewById(R.id.event_list_recycler_view);
    eventCards = ImNetwork.getEventCardList();
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    EventListAdapter adapter = new EventListAdapter(this, eventCards);
    recyclerView.setAdapter(adapter);
  }
}
