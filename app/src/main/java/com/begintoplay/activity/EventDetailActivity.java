package com.begintoplay.activity;

import android.os.Bundle;

import com.begintoplay.Logger;
import com.begintoplay.begintoplay.R;
import com.begintoplay.cardbuilder.CardFragmentFactory;
import com.begintoplay.cardtemplate.BaseTemplateModel;
import com.begintoplay.detailpage.EventDetailPageDTO;
import com.begintoplay.fragments.EventDetailFragment;
import com.begintoplay.fragments.IEventPageFragmentController;
import com.begintoplay.ui.BaseActivity;

public class EventDetailActivity extends BaseActivity implements IEventPageFragmentController {
  public static final String BUNDLE_EVENT_DATA = "bundle_event_data";

  private EventDetailPageDTO eventDetail;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    eventDetail = (EventDetailPageDTO) getIntent().getSerializableExtra(BUNDLE_EVENT_DATA);
    Logger.print("Event page activity : " + eventDetail);
    setContentView(R.layout.layout_base_activity);
    loadEventDetailFragment();
  }

  private void loadEventDetailFragment() {
    addFragmentInDefaultLayout(EventDetailFragment.newInstance(eventDetail), false);
  }

  @Override
  public int getFragmentContainerId() {
    return R.id.fragment_container;
  }

  @Override
  public void performOperation(int operation, Object input) {
    switch (operation) {
      case OPEN_CARD_DETAIL_PAGE:
        BaseTemplateModel model = (BaseTemplateModel) input;
        replaceFragmentInDefaultLayout(CardFragmentFactory.getFragment(model));
        break;
    }
  }
}
