package com.begintoplay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.begintoplay.begintoplay.R;
import com.begintoplay.detailpage.EventDetailPageDTO;
import com.begintoplay.presenter.HomePresenter;
import com.begintoplay.storage.ImDataStorage;
import com.begintoplay.view.HomeView;

public class HomeActivity extends AppCompatActivity implements HomeView {

  private HomePresenter presenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    presenter = new HomePresenter(this, new ImDataStorage());
  }

  public void onEventBtnClick(View view) {
    presenter.onEventClick(null);
  }

  @Override
  public void showDetailPage(EventDetailPageDTO eventDetailPageDTO) {
    Intent intent = new Intent(this, EventDetailActivity.class);
    intent.putExtra(EventDetailActivity.BUNDLE_EVENT_DATA, eventDetailPageDTO);
    startActivity(intent);
  }

  public void onEventListBtnClick(View view) {
    Intent intent = new Intent(this, EventListActivity.class);
    startActivity(intent);
  }
}
