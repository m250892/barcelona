package com.begintoplay.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.begintoplay.ImageLoader;
import com.begintoplay.Logger;
import com.begintoplay.begintoplay.R;
import com.begintoplay.cardbuilder.CardViewFactory;
import com.begintoplay.cardtemplate.BaseTemplateModel;
import com.begintoplay.detailpage.EventDetailPageDTO;
import com.begintoplay.detailpage.EventInfo;
import com.begintoplay.event.CardEvent;
import com.begintoplay.managers.EventManager;
import com.begintoplay.ui.BaseFragment;

import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDetailFragment extends BaseFragment {

  public static final String BUNDLE_EVENT_DATA = "bundle_event_data";

  @Bind(R.id.container)
  LinearLayout scrollContainer;

  private EventDetailPageDTO eventDetail;

  public EventDetailFragment() {
    // Required empty public constructor
  }

  @Override
  public void onResume() {
    super.onResume();
    EventManager.register(this);
  }

  @Override
  public void onPause() {
    EventManager.unregister(this);
    super.onPause();
  }

  public static EventDetailFragment newInstance(EventDetailPageDTO eventDetailPageDTO) {
    EventDetailFragment eventDetailFragment = new EventDetailFragment();
    Bundle args = new Bundle();
    args.putSerializable(BUNDLE_EVENT_DATA, eventDetailPageDTO);
    eventDetailFragment.setArguments(args);
    return eventDetailFragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    eventDetail = (EventDetailPageDTO) getArguments().getSerializable(BUNDLE_EVENT_DATA);
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_event_detail;
  }

  @Override
  protected void initViews(View baseView) {
    Toolbar toolbar = (Toolbar) baseView.findViewById(R.id.toolbar);
    setToolbar(toolbar);
    ImageView imageView = (ImageView) baseView.findViewById(R.id.cover_image_view);
    ImageLoader.getInstance().loadImage(eventDetail.getGallery().get(0), imageView);
    setEventInfo(baseView);
    loadCard();
  }

  private void loadCard() {
    Logger.print("Start Load cards in scroll container");
    for (BaseTemplateModel cardModel : eventDetail.getCards()) {
      View cardView = CardViewFactory.getView(cardModel, scrollContainer);
      if (cardView != null) {
        int extraMarginTop = this.getResources().getDimensionPixelSize(R.dimen.dimen_2dp);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) cardView.getLayoutParams();
        lp.setMargins(lp.leftMargin, lp.topMargin + extraMarginTop, lp.rightMargin, lp.bottomMargin);
        scrollContainer.addView(cardView, lp);
      }
    }
    Logger.print("End Load cards in scroll container");
  }

  private void setEventInfo(View baseView) {
    TextView title = (TextView) baseView.findViewById(R.id.title);
    TextView date = (TextView) baseView.findViewById(R.id.date);
    TextView price = (TextView) baseView.findViewById(R.id.price);
    EventInfo info = eventDetail.getInfo();
    title.setText(info.getName());
    date.setText(info.getDate());
    price.setText(info.getPrice());
  }

  @Subscribe
  public void onEvent(CardEvent event) {
    getFragmentController().performOperation(IEventPageFragmentController.OPEN_CARD_DETAIL_PAGE, event.getModel());
  }
}
