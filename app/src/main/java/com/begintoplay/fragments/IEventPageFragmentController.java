package com.begintoplay.fragments;

import com.begintoplay.ui.IBaseFragmentController;

/**
 * Created by manoj on 08/01/17.
 */

public interface IEventPageFragmentController extends IBaseFragmentController {
  int OPEN_CARD_DETAIL_PAGE = 1;
}
