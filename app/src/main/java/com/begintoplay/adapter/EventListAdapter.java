package com.begintoplay.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.begintoplay.ImageLoader;
import com.begintoplay.begintoplay.R;
import com.begintoplay.eventlist.EventCardDto;

import java.util.List;

/**
 * Created by manoj on 09/01/17.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventCardHolder> {

  private Context context;
  private List<EventCardDto> cardDtoList;

  public EventListAdapter(Context context, List<EventCardDto> cardDtoList) {
    this.context = context;
    this.cardDtoList = cardDtoList;
  }

  @Override
  public EventCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.layout_event_card, parent, false);
    return new EventCardHolder(view);
  }

  @Override
  public void onBindViewHolder(EventCardHolder holder, int position) {
    EventCardDto eventCardDto = cardDtoList.get(position);
    holder.title.setText(eventCardDto.getName());
    holder.date.setText(eventCardDto.getDate());
    holder.price.setText(eventCardDto.getPrice());
    ImageLoader.getInstance().loadImage(eventCardDto.getCoverUrl(), holder.coverImage);
    for (String tag : eventCardDto.getTagList()) {
      holder.tagContainer.addView(getTagView(tag, holder.tagContainer));
    }
  }

  @Override
  public int getItemCount() {
    return cardDtoList.size();
  }

  public class EventCardHolder extends RecyclerView.ViewHolder {

    private TextView title;
    private TextView date;
    private TextView price;
    private LinearLayout tagContainer;
    private ImageView coverImage;

    public EventCardHolder(View itemView) {
      super(itemView);
      title = (TextView) itemView.findViewById(R.id.title);
      date = (TextView) itemView.findViewById(R.id.date);
      price = (TextView) itemView.findViewById(R.id.price);
      tagContainer = (LinearLayout) itemView.findViewById(R.id.tag_container);
      coverImage = (ImageView) itemView.findViewById(R.id.cover_image_view);
    }
  }

  private View getTagView(String text, ViewGroup parent) {
    TextView tagView = (TextView) LayoutInflater.from(context).inflate(R.layout.tag_layout, parent, false);
    tagView.setText(text);
    return tagView;
  }
}
