package com.begintoplay.view;

import com.begintoplay.detailpage.EventDetailPageDTO;

/**
 * Created by manoj on 07/01/17.
 */

public interface HomeView {
  void showDetailPage(EventDetailPageDTO eventDetailPageDTO);
}
